export type Course = {
    id: String;
    title: String;
    description: String;
    authors: String;
    duration: String;
    createdDate: String;
}

export const dummyList: Course[] = [{
    id: '1',
    title: 'Angular',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    authors: 'Joe Gage',
    duration: '10:22 hours',
    createdDate: '10.10.2013',
},
{
    id: '2',
    title: 'JavaScript',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    authors: 'Joe Gage',
    duration: '14:22 hours',
    createdDate: '10.10.2013',
}]