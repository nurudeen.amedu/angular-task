import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-task-2';
  
  pageHeading: string = 'Your List is Empty'
  pageText: string = 'Please use `Add New Course` button to add your first course'
  addButtonText: string = 'ADD NEW COURSE'
  
  loggedIn: boolean = true
  userName: string = 'Joe Gage'
  headerButtonText: string = this.loggedIn ? 'LOGOUT' : 'LOGIN'
  
}
