import { Component, Input, Output } from '@angular/core';
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent {
  @Input() id: String = '';
  @Input() titleText: String = '';
  @Input() descriptionText: String = '';
  @Input() authorsText: String = '';
  @Input() durationText: String = '';
  @Input() createdDateText: String = '';
  @Input() canEditDelete: Boolean = false;

  @Output() onShowCourse = () => {
    console.log('show course'+this.id)
  }
  @Output() onEditCourse = () => {
    console.log('edit course'+this.id)
  }
  @Output() onDeleteCourse = () => {
    console.log('delete course'+this.id)
  }

  editIcon = faPen
  deleteIcon = faTrash

  buttonText: String = 'SHOW COURSE'
  canEdit: Boolean = false
}
